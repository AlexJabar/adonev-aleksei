package clock;

import java.awt.*;
import java.util.*;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        TimeTable schedule = new TimeTable();
        try {
            schedule.set("12:30", "14:00");
            schedule.set("12:45", "13:00");
            schedule.set("16:11", "17:00");
        } catch (IllegalArgumentException e) {
            System.out.println(e);
        }

            //schedule.findFirst("10:00", "17:00", "01:20");
            schedule.print();


    }
}



class TimeTable
{
    private List<Meeting> listOfMeetings;

    TimeTable() //конструктор
    {
        // создание расписания. объекты-метки в начале и в конце listOfMeetings потребуются
        //для цикла проверки свободных интервалов
        this.listOfMeetings = new LinkedList<Meeting>();
        this.listOfMeetings.add(new Meeting(0, 0,true));
        this.listOfMeetings.add(new Meeting(1440, 1440,true));
    }

    //создание встречи. помещается в конец списка.
    public void set(String start, String end) {

        int startMin = (60* Integer.parseInt(start.split(":")[0])) + Integer.parseInt(start.split(":")[1]);
        int endMin = (60* Integer.parseInt(end.split(":")[0])) + Integer.parseInt(end.split(":")[1]);

        for (int i = 0; i < this.listOfMeetings.size(); i++) {
            if (!this.listOfMeetings.get(i).isMarker) {
                   if (startMin > this.listOfMeetings.get(i).startMin && startMin < this.listOfMeetings.get(i).endMin
                       ||
                       endMin > this.listOfMeetings.get(i).startMin && endMin < this.listOfMeetings.get(i).endMin ) {

                       throw new IllegalArgumentException(out(startMin)+"..."+out(endMin)+" уже занято");
                   }
            }
        }
        
        this.listOfMeetings.add(new Meeting(startMin, endMin,false));
    }

    public void findFirst(String start, String end, String duration) {
        //парсинг ввода и перевод часов в минуты
        int flag = 0;
        int startMin = (60* Integer.parseInt(start.split(":")[0])) + Integer.parseInt(start.split(":")[1]);
        int endMin = (60* Integer.parseInt(end.split(":")[0])) + Integer.parseInt(end.split(":")[1]);
        int durationMin = (60* Integer.parseInt(duration.split(":")[0])) + Integer.parseInt(duration.split(":")[1]);

        // сортировка встреч по времени по возростанию
        Collections.sort(this.listOfMeetings, new Comparator<Meeting>() {

            @Override
            public int compare(Meeting o1, Meeting o2) {
                return o1.startMin < o2.startMin ? -1 : o1.startMin == o2.startMin ? 0 : 1;
            }
        });

        //основной цикл проверки интервалов.
        for (int i = 1; i < this.listOfMeetings.size() ; i++) {
            if (this.listOfMeetings.get(i).startMin > startMin) {

                if (startMin > this.listOfMeetings.get(i-1).endMin && startMin < this.listOfMeetings.get(i).startMin ) {
                    if (this.listOfMeetings.get(i).startMin - startMin >= durationMin) {
                        System.out.println("Есть свободное время с "
                                               +out(startMin)+" по "
                                               +out(startMin+durationMin));
                        flag = 1;
                        break;
                    }
                } else if (this.listOfMeetings.get(i).startMin - this.listOfMeetings.get(i-1).endMin >= durationMin) {
                        if (this.listOfMeetings.get(i - 1).endMin + durationMin <= endMin) {
                            System.out.println("Есть свободное время с "
                                    + out(this.listOfMeetings.get(i - 1).endMin) + " по "
                                    + out(this.listOfMeetings.get(i - 1).endMin + durationMin));
                            flag = 1;
                            break;
                        }
                }
            }
        }

        if (flag == 0) { // если остался 0, то "окна" не нашлось
            System.out.println("Свободных интервалов не найдено");
        }
    }

    // печать списка встреч без объектов-меток
    public void print() {

        // сортировка для удобства
        Collections.sort(this.listOfMeetings, new Comparator<Meeting>() {

            @Override
            public int compare(Meeting o1, Meeting o2) {
                return o1.startMin < o2.startMin ? -1 : o1.startMin == o2.startMin ? 0 : 1;
            }
        });
        System.out.println("Список встеч:");
        for (int i = 1; i < this.listOfMeetings.size()-1; i++) {
            System.out.println("С "+out(this.listOfMeetings.get(i).startMin)+" по "
                                   +out(this.listOfMeetings.get(i).endMin));
        }
    }

    // функция для вывода. минуты из int переводятся в часы с минутами в String
    private static String out(int min) {
        int hour = min/60;
        int minute = (min%60);
        String m,h;

        if (minute < 10) {
            m = "0"+String.valueOf(minute);
        } else m = String.valueOf(minute);

        if (hour < 10) {
            h = "0"+String.valueOf(hour);
        } else h = String.valueOf(hour);

        return h+":"+m;
    }


}

class Meeting // храним время в минутах дня
{
    int startMin;
    int endMin;
    boolean isMarker;

    public Meeting(int startMin, int endMin, boolean isMarker) {
        this.startMin = startMin;
        this.endMin = endMin;
        this.isMarker = isMarker;
    }
}