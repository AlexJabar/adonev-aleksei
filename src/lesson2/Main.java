package lesson2;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);


       System.out.print("Задача №1. Введите число 10:");
        int input = in.nextInt();
        if (input == 10) {
            System.out.println("Верно");
        } else System.out.println("Неверно");
//////////////////////////////////////////////////////////////////////////////////
        System.out.print("Задача №2. Введите число 0:");
        input = in.nextInt();
        if (input == 0) {
            System.out.println("Верно");
        } else System.out.println("Неверно");
////////////////////////////////////////////////////////////////////////////////
        System.out.print("Задача №3. Присвойте переменной Х значение true:");
        boolean input1 = in.nextBoolean();
        if (input1 == true) {
            System.out.println("Верно");
        } else System.out.println("Неверно");
///////////////////////////////////////////////////////////////////////
        System.out.print("Задача №4. Введите минуты от 0 до 59:");
         input = in.nextInt();
        if (input > 0 && input <= 15) {
            System.out.println("Это 1 четверть часа");
        } else if (input > 15 && input <= 30) {
            System.out.println("Это 2 четверть часа");
        } else if (input > 30 && input <= 45) {
            System.out.println("Это 3 четверть часа");
        } else if (input > 45 && input <= 59) {
            System.out.println("Это 4 четверть часа");
        }
////////////////////////////////////////////////////
        System.out.print("Задача №5. Введие чисо 1 или 2 или 3 или 4:");
        input = in.nextInt();
        switch (input) {
            case 1:
            {System.out.println("Зима"); break;}
            case 2:
            {System.out.println("Весна"); break;}
            case 3:
            {System.out.println("Лето"); break;}
            case 4:
            {System.out.println("Осень"); break;}
        }
////////////////////////////////////////////////////
        System.out.print("Задача №6. Введие месяц от 1 до 12:");
        input = in.nextInt();
        if (input == 12 || input == 1 || input == 2) {
            System.out.println("Это зима");
        } else if (input >= 3 && input <= 5) {
            System.out.println("Это весна");
        } else if (input >= 6 && input <= 8) {
            System.out.println("Это лето");
        } else if (input >= 9 && input <= 11) {
            System.out.println("Это осень");
        }
////////////////////////////////////////////////////
        System.out.print("Задача №7. Введие день от 1 до 31:");
        input = in.nextInt();
        if (input >=1 && input <=10) {
            System.out.println("Это 1 декада");
        } else if (input >= 11 && input <= 20) {
            System.out.println("Это 2 декада");
        } else System.out.println("Это 3 декада");

        System.out.print("Задача №8 (простые числа). Введите число:");
        input = in.nextInt();
        boolean flag = true;

        if(input==1) { // 1 - не простое число
            System.out.println("Число не простое");
            flag = false;
        }

        for(int d=2; d*d<=input; d++){
            // если разделилось нацело, то составное
            if(input%d==0) {
                System.out.println("Число не простое");
                flag = false;
                break;
            }

        }
        // если нет нетривиальных делителей, то простое
        if (flag) {
            System.out.println("Число простое");
        }






        System.out.print("Задача №9. Введите сторону треугольника");
         input = in.nextInt();


        String[][] tri = new String[input][input];

        for (int i=0; i<input; i++) {
            for (int j=0; j<input-i; j++) {

                    tri[i][j] = "*";
                    System.out.print(tri[i][j]);

            }
            System.out.println("");
        }




    }
}
