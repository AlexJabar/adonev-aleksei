package lesson5Arrays;

import java.text.MessageFormat;

public class Main {

    public static void main(String[] args) {
        float[] arr = {1, 2, 3, 4, 5, 6, -0.2f, -10, 8, 200};
        float[][] matrix = {{10, 23, -8}, {12, -5, 1}, {55, 1, 19}};
        /*task1(arr);
        task2(arr);
        task3(arr);
        task14(arr);*/
        //task26(matrix);

        inversion(matrix);




    }

    static void task1(float[] arr) {
        float min = arr[0];


        for (float item : arr) {
            if (item < min) {
                min = item;
            }
        }
        System.out.println(min);
    }


    ////////////////////////////////////////////// задача 2
    static void task2(float[] arr) {
        float max = arr[0];

        for (float item : arr) {
            if (item > max) {
                max = item;
            }
        }
        System.out.println(max);
    }

    ////////////////////////////////////////////// задача 3
    static void task3(float[] arr) {
        float sum = 0f;
        float med = 0;
        int quant = 0;
        for (float item : arr) {
            sum += item;
        }
        System.out.println(sum);
    }


    /////////////////////////////////////////////// задача 4
    static void task4(float[] arr) {
        float sum = 0f;
        float med = 0;

        for (float item : arr) {
            sum += item;
        }
        med = sum / arr.length;
        System.out.println(med);
    }

    ////////////////////////////////////////////// задача 5
    static void task5(float[] arr) {
        float mult = 1;
        for (float item : arr) {
            mult *= item;
        }
        System.out.println(mult);
    }

    ///////////////////////////////////// задача 6
    static void task6(float[] arr) {
        float sum = 0f;

        for (int i = 1; i < arr.length; i = i + 2) {
            sum += arr[i];
        }
        System.out.println(sum);
    }


    /////////////////////////////////////////////////////// задача 7
    static void task7(float[] arr) {
        float sum = 0f;

        for (int i = 0; i < arr.length; i = i + 2) {
            sum += arr[i];
        }
        System.out.println(sum);
    }


    ////////////////////////////////////////////////// задача 8
    static void task8(float[] arr) {
        float mult = 1f;

        for (int i = 1; i < arr.length; i = i + 2) {
            mult *= arr[i];
        }
        System.out.println(mult);
    }

    ////////////////////////////////////////////////// задача 9
    static void task9(float[] arr) {
        float mult = 1f;

        for (int i = 0; i < arr.length; i = i + 2) {
            mult *= arr[i];
        }
        System.out.println(mult);
    }


    ////////////////////////////////////////////////// задача 10
    static void task10(float[] arr) {
        int zero = 0;
        for (float item : arr) {
            if (item == 0) {
                zero++;
            }
        }
        System.out.println(zero);
    }

    ////////////////////////////////////////////////// задача 11

    static void task11(float[] arr) {
        int quant = 0;
        for (int i = 1; i < arr.length; i = i + 2) {
            quant++;
        }
        System.out.println(quant);
    }

    ////////////////////////////////////////////////// задача 12
    static void task12(float[] arr) {
        int quant = 0;
        for (int i = 0; i < arr.length; i = i + 2) {
            quant++;
        }
        System.out.println(quant);
    }

    ////////////////////////////////////////////////////// задача 13
    static void task13(float[] arr) {
        int k = 7;
        for (float item : arr) {
            if (item > k) {
                System.out.print(item + " ");
            }
        }
    }

    ////////////////////////////////////////////////////// задача 14
    static void task14(float[] arr) {
        int k = 7;
        for (float item : arr) {
            if (item < k) {
                System.out.print(item + " ");
            }
        }

    }


    ////////////////////////////////////////////////// задача 16
    static void task16(float[] arr) {
        float min = arr[0];
        for (float item : arr) {
            if (Math.abs(item) < Math.abs(min)) {
                min = item;

            }
        }
        System.out.println(min);
    }


    ////////////////////////////////////////////////// задача 17
    static void task17(float[] arr) {
        float max = arr[0];
        for (float item : arr) {
            if (Math.abs(item) > Math.abs(max)) {
                max = item;
            }
        }


        for (float item : arr) {
            item = item / max;
            System.out.print(item + " ");

        }
        System.out.println();
    }

////////////////////////////////////////////////// задача 18
    static void task18(float[][] matrix) {

        float min = matrix[0][0];

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                if (matrix[i][j] < min) {
                    min = matrix[i][j];
                }
            }

        }

        System.out.println(min);
    }

    ////////////////////////////////////////////////// задача 19
    static void task19(float[][] matrix) {

        float max = matrix[0][0];

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                if (matrix[i][j] > max) {
                    max = matrix[i][j];
                }
            }

        }
        System.out.println(max);
    }

    ////////////////////////////////////////////////// задача 20
    static void task20(float[][] matrix) {

        float sum = 0;
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                sum += matrix[i][j];
            }

        }
        System.out.println(sum);
    }

    ////////////////////////////////////////////////// задача 21
    static void task21(float[][] matrix) {

        float sum = 0;
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                sum += matrix[i][j];
            }

        }
        float med = (sum)/(matrix.length*matrix.length);
        System.out.println(med);
    }

    ////////////////////////////////////////////////// задача 22
    static void task22(float[][] matrix) {

        float sum = 0;
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                if (i == j) {
                    sum += matrix[i][j];
                }
            }

        }
        System.out.println(sum);
    }

    ////////////////////////////////////////////////// задача 23
    static void task23(float[][] matrix) {

        float sum = 0;
        for (int i = 0; i < matrix.length;  i++) {

                sum+=matrix[i][(matrix.length - i - 1)];

        }

        System.out.println(sum);
    }

    ////////////////////////////////////////////////// задача 24
    static void task24(float[][] matrix) {

        float det = matrix[0][0] * matrix[1][1] * matrix[2][2] + matrix[0][1] *
                matrix[1][2] * matrix[2][0] + matrix[0][2] * matrix[1][0] * matrix[2][1]
                - matrix[0][2] * matrix[1][1] * matrix[2][0] - matrix[0][0] * matrix[1][2] *
                matrix[2][1] - matrix[0][1] * matrix[1][0] * matrix[2][2];

        System.out.println(det);
    }


    //////////////////////////////////////// задача 25
    static void inversion(float [][]A)
    {
        double temp;
        int N = A.length;

        float [][] E = new float [N][N];


        for (int i = 0; i < N; i++)
            for (int j = 0; j < N; j++)
            {
                E[i][j] = 0f;

                if (i == j)
                    E[i][j] = 1f;
            }

        for (int k = 0; k < N; k++)
        {
            temp = A[k][k];

            for (int j = 0; j < N; j++)
            {
                A[k][j] /= temp;
                E[k][j] /= temp;
            }

            for (int i = k + 1; i < N; i++)
            {
                temp = A[i][k];

                for (int j = 0; j < N; j++)
                {
                    A[i][j] -= A[k][j] * temp;
                    E[i][j] -= E[k][j] * temp;
                }
            }
        }

        for (int k = N - 1; k > 0; k--)
        {
            for (int i = k - 1; i >= 0; i--)
            {
                temp = A[i][k];

                for (int j = 0; j < N; j++)
                {
                    A[i][j] -= A[k][j] * temp;
                    E[i][j] -= E[k][j] * temp;
                }
            }
        }

        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                A[i][j] = E[i][j];
            }
        }

        for (int i = 0; i < A.length; i++) {
            for (int j = 0; j < A[i].length; j++) {
                System.out.print(A[i][j]+"          ");
            }
            System.out.println();
        }

    }

    ////////////////////////////////////////////////// задача 26
    static void task26(float[][] matrix) {
        float max = Float.MIN_VALUE;
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                if (matrix[i][j] > max) {
                    max = matrix[i][j];
                }
            }
        }

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                matrix[i][j]/=max;
                System.out.print(matrix[i][j]+" ");
            }
            System.out.println();
        }
    }





////////////////////////////////////////////////// задача 27
    static void task27(float[][] matrix) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                System.out.print(matrix[j][i] + " ");
            }
            System.out.println();
        }
    }



    }
