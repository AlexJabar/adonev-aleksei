package vector;
import java.util.Arrays;
import java.util.Objects;
import java.lang.Object;

/*
В большой международной перспективной компании «Вектор» есть 4 департамента: департамент закупок, продаж, рекламы и логистики.
В этих 4 департаментах работают менджеры (ме), маркетологи (ма), инженеры (ин) и аналитики (ан).
Менеджер получает 500 тугриков в месяц, выпивает 20 литров кофе и производит 200 страниц отчетов в месяц
Маркетолог — 400 тугриков, 15 литров кофе и 150 страниц отчетов
Инженер — 200 тугриков, 5 литров кофе и 50 страниц чертежей
Аналитик — 800 тугриков и 50 литров кофе и 5 страниц стратегических исследований
Кроме того, все сотрудники бывают 3 рангов: первого, второго и третьего.
Сотрудник второго ранга получает на 25% больше, чем первого, а сотрудник 3-го ранга - на 50% больше, чем первого.
Для удобства, мы будем обозначать должность сокращенно, например менеджер 2-го ранга = ме2.

Вот состав департаментов:
Департамент закупок: 9×ме1, 3×ме2, 2×ме3, 2×ма1 + руководитель департамента ме2
Департамент продаж: 12×ме1, 6×ма1, 3×ан1, 2×ан2 + руководитель ма2
Департамент рекламы: 15×ма1, 10×ма2, 8×ме1, 2×ин1 + руководитель ма3
Департамент логистики: 13×ме1, 5×ме2, 5×ин1 + руководитель ме1
Руководитель получает на 50% больше, чем обычный сотрудник того же уровня, пьет в 2 раза больше кофе,
 и не производит отчетов, чертежей или стратегических исследований.

Задание: напиши программу для учета расходов и результатов работы всего дружного коддектива компании «Вектор».
Программа должна вывести:

-Число сотрудников в каждом департаменте
-Расходы на зарплату и на кофе по каждому департаменту и в сумме
-Число страниц документов и отчетов, которые производят каждый департамент и в сумме
-Посчитать средний расход тугриков на одну страницу

Программа должна быть сделана так, чтобы исходные данные о сотрудниках можно было легко поменять.
Должна быть возможность добавлять новые профессии, не меняя старые классы.
 */
public class Main {
    public static void main(String[] args) {
        // создание департаментов
            Department buy = makeBuy();//закупки
            Department sell = makeSell();//продаж
            Department adv = makeAdv();//рекламы
            Department log = makeLog();//логистики
        // создание комании
            Company company = new Company("Гугл");
            company.openDepartment(buy);
            company.openDepartment(sell);
            company.openDepartment(adv);
            company.openDepartment(log);

  // Подсчет и вывод статистики по всей компании
        System.out.printf("%23s%12s%12s%9s%9s%15s%n", "Департамент", "сотр.", "тугр.", "кофе", "стр.", "тугр./стр.");
        for (Department department:company.departmentsList) {
            System.out.printf("%23s", department.getDepartmentName());
            System.out.printf("%12d", department.getEmployeeNumber());
            System.out.printf("%,12.1f", department.getTotalDepartmentSalary());
            System.out.printf("%9d", department.getTotalDepartmentCoffe());
            System.out.printf("%9d", department.getTotalDepartmentReportPages());
            System.out.printf("%,15.1f", department.getTotalDepartmentSalary()/department.getTotalDepartmentReportPages());
            System.out.println();
        }
        System.out.println();

        /*           АНТИКРИЗИСНЫЕ МЕРЫ              */

/* 1. Сократить в каждом департаменте 40% (округляя в большую сторону) менеджеров, преимущественно самого низкого ранга.
Если менеджер является боссом, вместо него надо уволить другого менеджера, не босса. */

        buy.antiCrisisStep1();

        System.out.printf("%23s%12s%12s%9s%9s%15s%n", "Департамент", "сотр.", "тугр.", "кофе", "стр.", "тугр./стр.");
        for (Department department:company.departmentsList) {
            System.out.printf("%23s", department.getDepartmentName());
            System.out.printf("%12d", department.getEmployeeNumber());
            System.out.printf("%,12.1f", department.getTotalDepartmentSalary());
            System.out.printf("%9d", department.getTotalDepartmentCoffe());
            System.out.printf("%9d", department.getTotalDepartmentReportPages());
            System.out.printf("%,15.1f", department.getTotalDepartmentSalary()/department.getTotalDepartmentReportPages());
            System.out.println();
        }

    }


////////////////////////////////////// ОПИСАНИЕ КЛАССОВ и ФУНКЦИИ ////////////////////////////////////////

// наем работников в деп. закупки
    static Department makeBuy () {
        Department buy = new Department("Департамент закупок");
        for (int i = 0; i < 9; i++) {
            if (i < 1) {
                Manager manager = new Manager(500, 20, 200, 2, true);
                buy.hireEmployee(manager);
            }

            if (i < 2) {
                Manager manager = new Manager(500, 20, 200, 3, false);
                buy.hireEmployee(manager);
                Marketer marketer = new Marketer(400, 15, 150, 1, false);
                buy.hireEmployee(marketer);
            }

            if (i < 3) {
                Manager manager = new Manager(500, 20, 200, 2, false);
                buy.hireEmployee(manager);
            }

            Manager manager = new Manager(500, 20, 200, 1, false);
            buy.hireEmployee(manager);
        }
        return  buy;
    }
    // наем работников в деп. продаж
    static Department makeSell () {
        Department sell = new Department("Департамент продаж");

        for (int i = 0; i < 12; i++) {
            if (i < 1) {
                Marketer marketer = new Marketer(400, 15, 150, 2, true);
                sell.hireEmployee(marketer);
            }

            if (i < 2) {
                Analyst analyst = new Analyst(800, 50, 5, 2, false);
                sell.hireEmployee(analyst);

            }

            if (i < 3) {
                Analyst analyst = new Analyst(800, 50, 5, 1, false);
                sell.hireEmployee(analyst);
            }

            if (i < 6) {
                Marketer marketer = new Marketer(400, 15, 150, 1, false);
                sell.hireEmployee(marketer);
            }

            Manager manager = new Manager(500, 20, 200, 1, false);
            sell.hireEmployee(manager);
        }
        return  sell;
    }
    // наем работников в деп. рекламы
    static Department makeAdv() {
        Department adv = new Department("Департамент рекламы");

        for (int i = 0; i < 15; i++) {
            if (i < 1) {
                Marketer marketer = new Marketer(400, 15, 150, 3, true);
                adv.hireEmployee(marketer);
            }

            if (i < 2) {
                Engineer engineer = new Engineer(200, 5, 50, 1, false);
                adv.hireEmployee(engineer);

            }

            if (i < 8) {
                Manager manager = new Manager(500, 20, 200, 1, false);
                adv.hireEmployee(manager);
            }

            if (i < 10) {
                Marketer marketer = new Marketer(400, 15, 150, 2, false);
                adv.hireEmployee(marketer);
            }

            Marketer marketer = new Marketer(400, 15, 150, 1, false);
            adv.hireEmployee(marketer);
        }
        return  adv;

    }
    // наем работников в деп. логистики
    static Department makeLog() {
        Department log = new Department("Департамент логистики");

        for (int i = 0; i < 13; i++) {
            if (i < 1) {
                Manager manager = new Manager(500, 20, 200, 1, true);
                log.hireEmployee(manager);
            }

            if (i < 5) {
                Engineer engineer = new Engineer(200, 5, 50, 1, false);
                log.hireEmployee(engineer);
                Manager manager = new Manager(500, 20, 200, 2, false);
                log.hireEmployee(manager);

            }



            Manager manager = new Manager(500, 20, 200, 1, false);
            log.hireEmployee(manager);
        }
        return  log;
    }

}
///////////////////////////////////////////////////////////////////////////////////////////////////
abstract class Employee {
    protected double salary;
    protected int coffe;
    protected int reportPages;
    protected int rank;
    protected boolean isBoss;

    Employee(int salary, int coffe, int reportPages, int rank, boolean isBoss) {

           this.salary = salary;
           this.coffe = coffe;
           this.reportPages = reportPages;
           this.rank = rank;
           this.isBoss = isBoss;
       } // конструктор работника


    protected double getSalary(){
        double salary;
        switch (this.rank) {
            case 1: {
                salary = this.salary; break;
            }
            case 2: {
                salary = this.salary*1.25;  break;
            }
            case 3: {
                salary = this.salary*1.5;  break;
            }
            default: {
                throw new IllegalArgumentException("Недопустимый ранг");
            }
        }
        if (this.isBoss) {
            salary *= 1.5;
        }
        return salary;
    } // отдает ЗП в зависимоти от ранга

    protected int getCoffe() {
        int coffe;
        if (this.isBoss) {
               coffe  = this.coffe*2;
        } else coffe  = this.coffe;
        return coffe;
    } // отдает выпитое коффе

    protected int getReportPages() {
           int pages;
           if (this.isBoss) {
                  pages  = 0;
           } else pages  = this.reportPages;
           return pages;
       } // отдает листы работы

    protected int getRank() {
        return this.rank;
    }



   }
////////////////////////////////////////////////////////////////////////////////////////////////////
class Manager extends Employee {

    Manager(int salary, int coffe, int reportPages, int rank, boolean isBoss) {
        super(salary, coffe, reportPages, rank, isBoss);
    }


}

class Marketer extends Employee {

    Marketer(int salary, int coffe, int reportPages, int rank, boolean isBoss) {
        super(salary, coffe, reportPages, rank, isBoss);
    }
}

class Engineer extends Employee {

    Engineer(int salary, int coffe, int reportPages, int rank, boolean isBoss) {
        super(salary, coffe, reportPages, rank, isBoss);
    }
}

class Analyst  extends Employee {

    Analyst(int salary, int coffe, int reportPages, int rank, boolean isBoss) {
        super(salary, coffe, reportPages, rank, isBoss);
    }

}
//////////////////////////////////////////////////////////////////////////////////////////////////////

class Department
{
    private String name;
    public Employee[] employeesList;
    private int employeeNumber = 0;

    Department(String name) {
        this.name = name;
        this.employeesList = new Employee[1];
    }

    public void hireEmployee(Employee employee) {
        this.employeeNumber++;

        Employee[] buffer = this.employeesList.clone();// клонируем старый массив

        this.employeesList = new Employee[employeeNumber];// создаем новый массив под нового сотрудника


        for (int i = 0; i < this.employeeNumber-1; i++) { // переписываем из старого массива сотрудников в новый
            this.employeesList[i] = buffer[i];
        }
        this.employeesList[employeeNumber-1] = employee;

    }

    public int getEmployeeNumber() {
        int i = 0;
        for(Employee employee:this.employeesList) {
            i++;
        }

        return i;
    }

    public String getDepartmentName() {
        return this.name;
    }

    public double getTotalDepartmentSalary() {
        double sum = 0;
        for(Employee employee:this.employeesList) {
            sum += employee.getSalary();
        }
        return sum;
    }

    public int getTotalDepartmentCoffe() {
        int sum = 0;
        for(Employee employee:this.employeesList) {
            sum += employee.getCoffe();
        }
        return sum;
    }

    public int getTotalDepartmentReportPages() {
        int sum = 0;
        for(Employee employee:this.employeesList) {
            sum += employee.getReportPages();
        }
        return sum;
    }

   public void antiCrisisStep1() {
        // Выберем только менеджеров из департамента и создадим массив из них
        int managerNumber = 0;
    Employee[] managerArr = new Employee[0];// новый массив под менеджеров
    Employee[] managerBuffer = new Employee[0]; // резервный массив
        for (int i = 0; i < this.employeesList.length; i++) {
                if (this.employeesList[i] instanceof Manager && !this.employeesList[i].isBoss) {
                    managerNumber++;// найден +1 менеджер
                    managerBuffer = managerArr.clone(); // сохраняем старый массив
                    managerArr = new Employee[managerNumber]; // пересоздаем массив
                    for (int j = 0; j < managerNumber-1; j++) { // переписываем из старого массива менеджеров в новый
                        managerArr[j] = managerBuffer[j];
                    }
                    managerArr[managerNumber-1] = this.employeesList[i];
                }
        }
// сортируем менеджеров по рангу
       Employee temp;
       for(int i=0; i < managerArr.length; i++){
           for(int j=1; j < (managerArr.length-i); j++){
               if(managerArr[j-1].getRank() > managerArr[j].getRank()){
                   temp = managerArr[j-1];
                   managerArr[j-1] = managerArr[j];
                   managerArr[j] = temp;
               }
           }
       }

       int needToFire = (int) Math.ceil(managerArr.length * 0.4);// уволить 40% менеджеров с округлением до большего
       Employee[] firedManagerArr = new Employee[needToFire];

       for (int i = 0; i < firedManagerArr.length ; i++) { // создаем массив из тех кто должен быть уволен
           firedManagerArr[i] = managerArr[i];
       }

       for (int i = 0; i < this.employeesList.length ; i++) { // перебераем всех сотрудников департамента
           if (this.employeesList[i] instanceof Manager) { //
               for (int j = 0; j < firedManagerArr.length; j++) {
                   if (firedManagerArr[j].equals(this.employeesList[i])) {// если есть в списке на увольнение...
                       this.employeesList[i] = null;// увольняем
                   }
               }
           }
       }
       // в этом сегменте мы пересобираем массив всех сотрудников с учетом уволбнений
       Employee[] buffer = this.employeesList.clone();
        this.employeesList = new Employee[buffer.length - needToFire];
        int i =0;
       for (Employee manager:buffer) {
           if (manager != null) {
               this.employeesList[i] = manager;
               i++;
           }
       }
    } // сокращения сотрудников


}

class Company
{
    private String name;
    public Department[] departmentsList;
    private int departmentNumber = 0;

    Company(String name) {
        this.name = name;
        this.departmentsList = new Department[1];
    }

    public void openDepartment(Department department) {// все по принципу наему работников в департаменты
        this.departmentNumber++;

        Department[] buffer = this.departmentsList.clone();

        this.departmentsList = new Department[departmentNumber];


        for (int i = 0; i < this.departmentNumber-1; i++) {
            this.departmentsList[i] = buffer[i];
        }
        this.departmentsList[departmentNumber-1] = department;

    } // по аналогии с наемом сотрудников в департаменты
}