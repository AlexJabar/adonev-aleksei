package lesson15;

import java.util.Arrays;
import java.util.*;
import java.util.Scanner;


public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        //task1(5);
        //task2(12,3);
        //task3(14,14);
        //System.out.println(task4(56));

        //int a = 945;
        //int length = String.valueOf(a).length();

        //System.out.println((a% (int) Math.pow(10,length))/((int) Math.pow(10,length-1)));
        //task8("topotr");
        //String alef = "sfsdfsdf";
        //alef.tr
        //int n = 12345678;
        //System.out.println(n % (int) Math.pow(10, (int) Math.log10(n)));
        //task9(in);
    }



    static void task1(int n) {
        if (n==1) {
           System.out.println(n);
        } else {
            System.out.println(n);
            task1(n-1);
        }
    }

//вывод интервала
    static void task2(int a, int b) {

        if (a <= b) {
            if (a == b) {
                System.out.println(b);
            } else {
                System.out.println(a);
                task2(a+1,b);
            }
        } else {
            if (a == b) {
                System.out.println(b);
            } else {
                System.out.println(a);
                task2(a-1,b);
            }
        }
    }

    static int task3(int m, int n) {
        System.out.print("m="+m+" n="+n);
        System.out.println();
        if (m == 0) {

            n++;
        } else if (m > 0 && n == 0) {

            task3(m-1, 1);
        } else if (m > 0 && n > 0) {

            task3(m-1, task3(m, n-1));
        }

        return 0;
    }

    static int task4(int x) {
      if (x < 10) {
          return x;
      } else {
          return x%10 + task4(x/10);
      }


    }

    static void task5(int x) {
        if (x < 10) {
            System.out.println(x);
        } else {
            System.out.println(x % 10);
            task5(x / 10);
        }
    }

    static void task6(int x) {
        int length = String.valueOf(x).length();
        if (x < 10) {
            System.out.println(x);
        } else {
            System.out.println((x% (int) Math.pow(10,length))/((int) Math.pow(10,length-1)));
            task6(x % (int) Math.pow(10, (int) Math.log10(x)));
        }
    }

    static void task8(String str) {

        if (str.length() == 0 || str.length() == 1) {
            System.out.println("YES");
        } else if(str.substring(0, 1).equals(str.substring(str.length() - 1))) {
            str = str.substring(1, str.length()-1);
            task8(str);
        } else System.out.println("NO");


    }

    /*static int task7 (int n) {
        if (n == 1) {
            System.out.println("end");
        }


    }*/



}
