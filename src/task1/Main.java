package task1;

public class Main {

    public static void main(String[] args) {

        /*задача про молнию*/
        int speedOfSoundft = 1100;
        float time = 7.2f;

        float distance = speedOfSoundft * time;
        System.out.println("Расстояние до молнии - "+distance+" футов");

        /*задача про луну*/
        final float MOON_COEFFICIENT = 0.17f;
        int mass = 69;

        float moonWeight = mass * MOON_COEFFICIENT;
        System.out.println("Мой вес на луне примерно - "+moonWeight+" кг");
    }
}
