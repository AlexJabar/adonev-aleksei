package lesson7;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        int[] arrInt = {1,21,21,21, 21, 1,3};
        double[] arrDouble = {1,2, 3,5,2,1,423,423,423,4,2,34,2,2};

        double[][] matrix = {{ 1 , 6, 8},
                             { -4, -18,  12},
                             { 13, 9,   2}
        };

        double[][][] test = {
                {{3, 3, 3},
                 {3, 3, 31}},

                {{2, 2, 2},
                 {2, 2, 2}},

                {{1, 1, 1},
                {1, 1, 1}},
        };





        //char[] symb = task1();
        //double ans5 = task5(matrix);
        //System.out.print(ans5);

        //int[][][] ans6 = task6(3,3,4);
        //System.out.print(ans6[2][2][3]);

        //int ans = task4(arrInt);
        //System.out.print(ans);

        //boolean ans7 = task7(arrInt);
        //System.out.print(ans7);

        //double ans8 = task8(arrDouble);
        //System.out.print(ans8);

        //boolean ans9 = task9(arrDouble);
        //System.out.print(ans9);

        //double[] ans10 = task10(arrDouble);
        //for (double item:ans10) {
            //System.out.print(item + "   ");
        //}

        /*double[] ans11 = task11(arrDouble);
        for (double item:ans11) {
            System.out.print(item+"  ");
        }*/

        /*double[] ans12 = task12(arrDouble);
        for (double item:ans12) {
            System.out.print(item+"  ");
        }*/

        //double ans14 = task14(arrDouble);
        //System.out.println(ans14);

        //boolean ans15 = task15(arrDouble);
        //System.out.println(ans15);

        //int ans16 = task16(arrDouble);
        //System.out.println(ans16);

        //task22(matrix);
        //task23(matrix);

        //double ans25 = task25(test);
        //System.out.println(ans25);

        //double[][][] ans26 = task26(test);
        //out3D(ans26);

        //double ans27 = task27(test);
        //System.out.println(ans27);

        //double[][][] ans28 = task28(test);
        //out3D(ans28);

        //task29(test);

        //task30(test);
        task34(test);





    }

//задача 1
    static char[] task1() {
        char[] symb = {'a','b','c','d','e','f','g','h','i','j','k',
                        'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v','w','x','y','z'};
        return symb;
    }
  // задача 2
  static int task2(int[] arr) {
        int max = Integer.MIN_VALUE;
        for (int item:arr) {
           max = item > max ? item : max;
        }
        return max;
  }
    // задача 3
    static int task3(int[] arr) {
        int min = Integer.MAX_VALUE;
        for (int item:arr) {
            min = item < min ? item : min;
        }
        return min;
    }
    // задача 4
    static int task4(int[] arr) {

        int sum = 0;
        for (int i =0; i < arr.length-1; i++) {
            if (arr[i] == 1) {
                sum+=arr[i+1];
            }
        }
        return sum;
    }
    // задача 5
    static double task5(double[][] matrix) {
        double min = matrix[0][0];
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                if (i == j) {
                    min = matrix[i][j] < min ? matrix[i][j] : min;
                }
            }

        }

    return min;
    }
    // задача 6
    static int[][][] task6(int n, int m,int k) {
        int[][][]  threeDArray = new int[n][m][k];
        int i = 0;
        int j = 0;
        int f = 0;

        while (i < n) {
            j=0;
            while (j < m) {
                f=0;
                while (f < k) {
                    threeDArray[i][j][f] = 1;
                    f++;
                }
             j++;
            }
         i++;
        }

        return threeDArray;
    }
    // задача 7
    static boolean task7(int[] arr) {
        int standard = arr[0];
        boolean flag = true;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] != standard){
                flag = false;
                break;
            }
        }
        return flag;
    }
    // задача 8
    static double task8(double[] arr) {
        double sum = 0;
        for (int i = 0; i < arr.length ; i++) {
            sum = arr[i] > 0 ? sum + arr[i]: sum + 0;
        }

        return sum;
    }
    // задача 9
    static boolean task9(double[] arr) {
        boolean flag = true;
        for (int i = 1; i < arr.length ; i++) {
            if (arr[i-1] > arr[i]) {
                flag = false;
                return flag;
            }
        }
        return flag;
    }
    // задача 10
    static double[] task10(double[] arr) {
        double[] arrMax = new double[3];
        double temp = 0;

        for(int i=0; i < arr.length; i++){
            for(int j=1; j < (arr.length-i); j++){
                if(arr[j-1] < arr[j]){
                    temp = arr[j-1];
                    arr[j-1] = arr[j];
                    arr[j] = temp;
                }

            }
        }

        for (int i = 0; i < 3; i++) {
            arrMax[i] = arr[i];
        }
        return arrMax;
    }
    // задача 11
    static double[] task11(double[] arr) {
        double[] reversedArr = new double[arr.length];
        for (int i = arr.length-1, j=0; i >= 0 ; i--, j++) {
            reversedArr[j] = arr[i];
        }
        return  reversedArr;
    }
    // задача 12
    static double[] task12(double[] arr) {
        double[] shiftedArr = new double[arr.length];
        for (int i = 0; i < arr.length ; i++) {
            if (i == 0) {
                shiftedArr[i] = arr[arr.length-1];
            } else {
                shiftedArr[i] = arr[i - 1];
            }
        }
        return shiftedArr;
    }
    // задача 14
    static double task14(double[] arr) {
        double max = Integer.MIN_VALUE;
        for (int i = 0; i < arr.length; i++) {
            max = arr[i] > max ? arr[i]: max;
        }

        return max;
    }
    // задача 15
    static boolean task15(double[] arr) {
        boolean flag = true;
        for (int i = 0; i < arr.length ; i++) {
            for (int j = 0; j < arr.length; j++) {
                if(i!=j) {
                    if (arr[i] == arr[j]) {
                        flag = false;
                        return flag;
                    }
                }
            }

        }
        return flag;
    }

    static int task16(double[] arr) {
        double temp = 0;
        int counter = 0;

        for (int i = 0; i < arr.length ; i++) { // сначала сортируем  по возростанию
            for (int j = 1; j < arr.length - i ; j++) {
                if (arr[j-1] > arr[j]) {
                    temp = arr[j-1];
                    arr[j-1] = arr[j];
                    arr[j] = temp;
                }
            }

        }
// теперь считаем количество изменений числа
        for (int i = 0; i < arr.length-1; i++) {
            if (arr[i] != arr[i+1]) {
                counter++;
            }
        }
        counter++;// не забываем про 1 число
        return counter;
    }

    static int task17(double[] arr) {

        int counter = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == 0) {
                counter++;
            }
        }
        counter++;// не забываем про 1 число
        return counter;
    }

    static int task18(double[] arr) {
        int k = 1;
        int counter = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] < k ) {
                counter++;
            }
        }
        counter++;
        return counter;
    }

    static double[] task19(double[] arr) {
        double sum = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] >= 0 ) {
                sum+=arr[i];
            }
        }

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] >= 0 ) {
                arr[i] /= sum;
            }
        }

        return arr;
    }

    static double task20(double[][] arr) {
        double sum = 0;

        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                sum += arr[i][j];
            }
        }
        return sum;
    }

    static double task21(double[][] arr) {
        double sum = 0;
        int counter = 0;

        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                sum += arr[i][j];
                counter++;
            }
        }
        return sum/counter;
    }

    static void task22(double[][] arr) {
        double sumMain = 0;
        double sumSide = 0;

        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                if (i == j) {
                    sumMain += arr[i][j];
                }
                if (i + j == arr.length -1) {
                    sumSide += arr[i][j];
                }
            }
        }

        System.out.print("сумма на главной "+sumMain+", сумма на побочной "+sumSide);
    }

    static double[][] task23(double[][] arr) {
        double max = arr[0][0];

        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                if (Math.abs(arr[i][j]) > Math.abs(max)) {
                    max = arr[i][j];
                }
            }
        }

        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                arr[i][j] /= max;
            }
        }

        return arr;
    }

    static double[][] task24(double[][] arr) {

        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                if (Math.abs(arr[i][j]) > 10)
                {
                    arr[i][j] /= 2;
                }
            }
        }

        return arr;
    }


    //задача 25
    static double task25(double[][][] arr) {
        double sum = 0;
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                for (int k = 0; k < arr[i][j].length; k++) {
                    sum+=arr[i][j][k];
                }
            }
        }
        return sum;
    }
    //задача 26
    static double[][][] task26(double[][][] arr) {
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                for (int k = 0; k < arr[i][j].length; k++) {
                    arr[i][j][k] = arr[i][j][k] < 0 ? arr[i][j][k] * (-1): arr[i][j][k];
                }
            }
        }
        return arr;
    }
    //задача 27
    static double task27(double[][][] arr) {
        double sum = 0;
        int counter = 0;
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                for (int k = 0; k < arr[i][j].length; k++) {
                    if (arr[i][j][k] >= 0) {
                        sum += arr[i][j][k];
                        counter++;
                    }
                }
            }
        }
        double med = sum/counter;
        return med;
    }

    static void out3D(double[][][] arr) {
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                for (int k = 0; k < arr[i][j].length; k++) {
                    System.out.print(arr[i][j][k]+"   ");
                }
                System.out.println();
            }
            System.out.println();
        }
    }


    //задача 28
    static double[][][] task28(double[][][] arr) {
        // делаем из матрицы простой массив
        int result = 0;


        double[] arr1D = new double[(arr[0][0].length * arr[0].length * arr.length)];
        int q = 0;
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                for (int k = 0; k < arr[i][j].length ; k++) {
                    arr1D[q] = arr[i][j][k];
                    q++;
                }
            }
        }
        // сортировка пузырьком
        double temp = 0;
        for (int i = 0; i < arr1D.length; i++) {
            for (int j = 1; j < (arr1D.length - i); j++) {
                if (arr1D[j - 1] > arr1D[j]) {
                    temp = arr1D[j - 1];
                    arr1D[j - 1] = arr1D[j];
                    arr1D[j] = temp;
                }

            }
        }
        // строим матрицу из массива
        q = 0;
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                for (int k = 0; k < arr[i][j].length ; k++) {
                    arr[i][j][k] = arr1D[q];
                    q++;
                }
            }
        }

        return arr;

    }


    //задача 29
    static void task29(double[][][] arr) {
        double[] sumArray = new double[arr.length]; // создаем массив под суммы плоскостей.
        double sum = 0;


        // вычисляем суммы для каждой плоскости.  индексы sumArray соотв. номерам плоскостей
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                for (int k = 0; k < arr[i][j].length; k++) {
                    sum += arr[i][j][k];
                }
            }
            sumArray[i] = sum;
            sum = 0;
        }

// сортируем 3-мерный массив методом вставок, на основании массива сум sumArray
        double temp = 0;
    for (int q =0 ; q <  arr.length ; q++) {
        for (int i = q + 1; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                for (int k = 0; k < arr[i][j].length; k++) {
                    if (sumArray[q] > sumArray[i]) { //если след. сумма меньше текущей, то меняем местами плоскости
                        temp = arr[q][j][k];
                        arr[q][j][k] = arr[i][j][k];
                        arr[i][j][k] = temp;
                    }
                }
            }
        }
    }

    out3D(arr);


    }

    static void task30(double[][][] arr) {

        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                for (int k = 0; k < arr[i][j].length; k++) {
                    System.out.print(arr[i][j][k]+" ");
                }
            }
        }




    }

    static void task31(double[][][] arr) {
        double[] sumArray = new double[arr.length]; // создаем массив под суммы плоскостей.
        double sum = 0;
        double max = Integer.MIN_VALUE;
        int index = 0; // нужно хранить первый индекс максимального значения
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                for (int k = 0; k < arr[i][j].length; k++) {
                    if (arr[i][j][k] > max) {
                        max = arr[i][j][k];
                        index = i;
                    }
                }
            }
        }

        double[][] arr2D = new double[arr[0].length][arr[0][0].length];

        for (int i = 0; i < arr2D.length ; i++) {
            for (int j = 0; j < arr2D[i].length ; j++) {
                arr2D[i][j] = arr[index][i][j]; //переписываем элементы 3д массива в 2д, используя максимального элемента
            }
        }
        for (int i = 0; i < arr2D.length ; i++) {
            for (int j = 0; j < arr2D[i].length ; j++) {
                System.out.print(arr2D[i][j]+" ");
            }
            System.out.println();
        }











    }

    static void task32(double[][][] arr) {
        for (int i = 0; i < arr.length; i++) {
            System.out.print("   z-"+i+": ");
            for (int j = 0; j < arr[i].length ; j++) {
                for (int k = 0; k < arr[i][j].length; k++) {
                    System.out.print(arr[i][j][k]+"  ");
                }

            }

        }
    }


    static void task33(double[][][] arr) {
        double sum = 0;
        int counter = 0;
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                for (int k = 0; k < arr[i][j].length; k++) {
                    sum+=arr[i][j][k];
                    counter++;
                }
            }
            System.out.print(sum/counter+", ");
            sum=0;
            counter=0;
        }
    }

    static void task34(double[][][] arr) {
        double[] sumArray = new double[arr.length]; // создаем массив под С.А. плоскостей.
        double sum = 0;
        int counter = 0;

        // вычисляем суммы для каждой плоскости.  индексы sumArray соотв. номерам плоскостей
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                for (int k = 0; k < arr[i][j].length; k++) {
                    sum += arr[i][j][k];
                    counter++;
                }
            }
            sumArray[i] = sum/counter;
            sum = 0;
            counter =0;
        }

        // сортируем 3-мерный массив методом вставок, на основании массива сум sumArray
        double temp = 0;
        for (int q = 0; q < arr.length; q++) {
            for (int i = q + 1; i < arr.length; i++) {
                for (int j = 0; j < arr[i].length; j++) {
                    for (int k = 0; k < arr[i][j].length; k++) {
                        if (sumArray[q] > sumArray[i]) { //если след. сумма меньше текущей, то меняем местами плоскости
                            temp = arr[q][j][k];
                            arr[q][j][k] = arr[i][j][k];
                            arr[i][j][k] = temp;
                        }
                    }
                }
            }
        }

        out3D(arr);
    }
}


