package Bhina2;

public class Main {
    public static void main(String[] args) {
        Hamster bob = new Hamster("M","red","25");
        Chinchilla sam = new Chinchilla("M","red","25");
        bob.run();
        sam.run();
    }
}

abstract class Rodent
{
    protected String gender;
    protected String color;
    protected String speed;

    public Rodent(String gender, String color, String speed) {
        this.gender = gender;
        this.color = color;
        this.speed = speed;
    }

    public void run() {
        System.out.println(this.getClass().getSimpleName()+" "+this.speed);
    }

}

class Hamster extends Rodent
{
    public Hamster(String gender, String color, String speed) {
        super(gender, color, speed);
    }


}

class Chinchilla extends Rodent
{
    public Chinchilla(String gender, String color, String speed) {
        super(gender, color, speed);
    }
}

class Rat extends Rodent
{
    public Rat(String gender, String color, String speed) {
        super(gender, color, speed);
    }
}
