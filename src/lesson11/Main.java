package lesson11;


import java.util.Scanner;
import java.util.Random;

public class Main {

    public static void main(String[] args) {

        Human human = new Human();
        Computer computer = new Computer();
        Game game = new Game(human,computer);
        game.play();




    }
}

enum Move {Paper,Rock,Scissors};

interface Player
{
    String getName();
    Move getMove(Scanner in);
    void addPoint();
    int getPoints();

}

class Human implements Player
{
    private String name;
    private int points;

    Human(){

    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public Move getMove(Scanner in) {
        Move move = Move.valueOf(in.next());
        return move;
    }

    @Override
    public void addPoint() {
        this.points++;
    }

    @Override
    public int getPoints() {
        return this.points;
    }

    public void setName(String name) {
        this.name = name;
    }
}

class Computer implements Player
{
    private String name = "Robot";
    private int points;

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public Move getMove(Scanner in) {
        return Move.values()[new Random().nextInt(Move.values().length)];
    }

    @Override
    public void addPoint() {
        this.points++;
    }

    @Override
    public int getPoints() {
        return this.points;
    }
}

class Game {

    private Human human;
    private Computer computer;

    Game(Human human, Computer computer) {
        this.human = human;
        this.computer = computer;
    }

        public void play() {
            Scanner in = new Scanner(System.in);
            System.out.println("Введите ваше имя: ");
            String name = in.next();
            this.human.setName(name);
            while (true) {
                System.out.println("Ваш ход: ");

                Move move = human.getMove(in);




                Move compMove = computer.getMove(null);
                System.out.println(compMove);

                if (move == Move.Paper && compMove == Move.Rock) {
                    System.out.println("Побидил " + human.getName());
                    human.addPoint();
                } else if (move == Move.Rock && compMove == Move.Paper) {
                    System.out.println("Побидил " + computer.getName());
                    computer.addPoint();
                } else if (move == Move.Rock && compMove == Move.Scissors) {
                    System.out.println("Побидил " + human.getName());
                    human.addPoint();
                } else if (move == Move.Scissors && compMove == Move.Rock) {
                    System.out.println("Побидил " + computer.getName());
                    computer.addPoint();
                } else if (move == Move.Scissors && compMove == Move.Paper) {
                    System.out.println("Побидил " + human.getName());
                    human.addPoint();
                } else if (move == Move.Paper && compMove == Move.Scissors) {
                    System.out.println("Побидил " + computer.getName());
                    computer.addPoint();
                } else System.out.println("Ничья");

                System.out.println("Очков у " + human.getName() + " -" + human.getPoints());
                System.out.println("Очков у " + computer.getName() + " -" + computer.getPoints());
                System.out.println("Wanna play again?");
                String answer = in.next();
                if (answer.equals("Yes")) {
                    continue;
                } else break;
            }
            System.out.println("game over");
        }
    }

