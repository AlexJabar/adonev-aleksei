package lesson3;

public class Main {

    public static void main(String[] args)
    {
        double maxTMP =0;
        double max = 0;

        for (double x = 0 ; x <= 10; x+=0.01) {
            maxTMP = Math.pow(x,2);

            if (maxTMP > max) {
                max = maxTMP;
            }
        }

        System.out.println(Math.round(max));

        ///////////////////////////////////////////////////

         maxTMP =0;
         max = 0;

        for (double x = -15 ; x <= 15; x+=0.01) {
            maxTMP = (Math.pow(x,2) - x + 3);

            if (maxTMP > max) {
                max = maxTMP;
            }
        }
        System.out.println(Math.round(max));
        /////////////////////////////////////
        maxTMP =0;
        max = 0;

        for (double x = -10 ; x <= 10; x+=0.01) {
            maxTMP = (Math.sin(x)+Math.pow(x,2));

            if (maxTMP > max) {
                max = maxTMP;
            }
        }
        System.out.println(Math.round(max));
        ///////////////////////////////////////////////////
        maxTMP =0;
        max = 0;

        for (double x = -10 ; x <= 10; x+=0.01) {
            maxTMP = (Math.cos(x)+Math.pow(x,2));

            if (maxTMP > max) {
                max = maxTMP;
            }
        }
        System.out.println(Math.round(max));

        maxTMP =0;
        max = 0;

        for (double x = -50 ; x <= 50; x+=0.01) {
            maxTMP = (Math.pow(x,3));

            if (maxTMP > max) {
                max = maxTMP;
            }
        }
        System.out.println(Math.round(max));
        //////////////////////////////////////////////////////////////
        maxTMP =0;
        max = 0;

        for (double x = -10; x <= 10 ;x+=0.01) {
            if (x > -1 && x < 1) {
                continue;
            } else {
                maxTMP = (1 / Math.pow(x, 3));

                if (maxTMP > max) {
                    max = maxTMP;
                }
            }

        }
        System.out.println(Math.round(max));

        maxTMP =0;
        max = 0;

        for (double x = -10; x <= 10 ;x+=0.01) {
            if (x > -1 && x < 1) {
                continue;
            } else {
                maxTMP = (Math.pow(x, 3) + Math.pow(x, 3));

                if (maxTMP > max) {
                    max = maxTMP;
                }
            }

        }
        System.out.println(Math.round(max));

        maxTMP =0;
        max = 0;

        for (double x = -10 ; x <= 10; x+=0.01) {
            maxTMP = (Math.pow(Math.E,x));

            if (maxTMP > max) {
                max = maxTMP;
            }
        }
        System.out.println(Math.round(max));

        maxTMP =0;
        max = 0;

        for (double x = -100 ; x <= 100; x+=0.01) {
            maxTMP = (Math.pow(Math.E,Math.sin(x)));

            if (maxTMP > max) {
                max = maxTMP;
            }
        }
        System.out.println(Math.round(max));

        maxTMP =0;
        max = 0;

        for (double x = -10 ; x <= 10; x+=0.01) {
            maxTMP = (Math.pow(x,Math.E));

            if (maxTMP > max) {
                max = maxTMP;
            }
        }
        System.out.println(Math.round(max));

        maxTMP =0;
        max = 0;

        for (double x = -5 ; x <= 5; x+=0.01) {
            maxTMP = (Math.pow(x,Math.E)+x);

            if (maxTMP > max) {
                max = maxTMP;
            }
        }
        System.out.println(Math.round(max));

        maxTMP =0;
        max = 0;

        for (double x = -5 ; x <= 5; x+=0.01) {
            maxTMP = (Math.pow(x,Math.E)+Math.pow(x,2));

            if (maxTMP > max) {
                max = maxTMP;
            }
        }
        System.out.println(Math.round(max));






    }
}
