package test1;

import javax.xml.bind.Element;
import java.util.*;

import  java.lang.Class;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {

    public static void main(String[] args) {

    /*StringKeyArray<String,Integer> arr = new StringKeyArray();
        arr.addElement("a",1);
        arr.addElement("b",2);
        arr.addElement("c",3);
        arr.addElement("d",4);
        arr.addElement("e",5);
        arr.addElement("f",6);
        arr.addElement("g",7);
        arr.addElement("j",8);
    try {
        System.out.println(arr.getElementByKey("g"));
    } catch (IllegalArgumentException e ) {
        System.out.println(e);
    }*/

        String a = "Hey wollef sroirraw";
        String regex = "(?<=[\\s\\S])(\\S{5,})(?= |$)";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(a);

        while (matcher.find()) {
            a = a.replaceAll(regex, matcher.group(0));
        }


        System.out.println(a);

}
}


class StringKeyArray<keyType,valueType>
{
    public Element[] elements = new Element[2];
    private int elementsNumber = 0;

    public void addElement(keyType key, valueType value) {

        if (this.elementsNumber == this.elements.length-1) {
            Element[] buffer = new Element[this.elements.length];
            System.arraycopy(this.elements,0,buffer,0,this.elements.length);
            this.elements = new Element[this.elements.length*2];
            System.arraycopy(buffer,0,this.elements,0,buffer.length);

        }

        Element elementNew = new Element(key, value);
        this.elements[elementsNumber] = elementNew;
        this.elementsNumber++;
    }


    public valueType getElementByKey(keyType key) {
        for(Element element:elements) {
            if (element != null) {
                if (element.key.equals(key)) {
                    return (valueType) element.value;
                }
            }
        }
        throw new IllegalArgumentException("Такого ключа нет");
    }


    class Element<keyType,valueType>
    {
       int index;
       keyType key;
       valueType value;

        Element(keyType key, valueType value) {
            this.key = key;
            this.value = value;
            this.index = elementsNumber;
        }
    }
}






