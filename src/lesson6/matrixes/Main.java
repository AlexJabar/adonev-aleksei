package lesson6.matrixes;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        double[][] twoDimArr = {{ 15.0,     4,        9 },
                                { 7.0,      5.9,      8 },
                                { 8.1,      10,       7 },
                                { 1.1,       5,       6  },};

        double[][] TwoDimArr1 = {    {1, 2, },
                                     {3, 4, },
                                     {2, 3,}};

        double[][] TwoDimArr2 = {{1, 2, 3, 4},
                                 { 1, 3, 4, 2},
                                               };

        int[][] IntegerTwoDimArr = { {1, 2, },
                                    {3, 4, },
                                    {2, 3,}};

        double[][]sorted1 = sort1(twoDimArr);
        out(sorted1);

        double[][] sorted3 = sort3(twoDimArr);
        out(sorted3);

        double[][] sorted5 = sort5(twoDimArr);
        out(sorted5);

        int rows = sort7(IntegerTwoDimArr);

        int cols = sort8(IntegerTwoDimArr);

        try {
            double[][] resMatr = matrixMult(TwoDimArr1, TwoDimArr2);
            out(resMatr);
        } catch(IllegalArgumentException e ) {
            System.out.print(e);
        }







    }

    // вывод массива в консоли
    static void out(double[][] twoDimArr) {
        for (int i = 0; i < twoDimArr.length; i++) {
            for (int j = 0; j < twoDimArr[i].length; j++) {
                System.out.print(twoDimArr[i][j]+"          ");
            }
            System.out.println();
        }
        System.out.println();
    }

    // сортировка элементов массива (задание 1 и 2)
    static double[][] sort1(double[][] twoDimArr) {
// делаем из матрицы простой массив
        int result = 0;


        double[] arr = new double[twoDimArr[0].length * twoDimArr.length];
        int k = 0;
        for (int i = 0; i < twoDimArr.length; i++) {
            for (int j = 0; j < twoDimArr[i].length; j++) {
                arr[k] = twoDimArr[i][j];
                k++;
            }
        }
// сортировка пузырьком
        double temp = 0;
        for(int i=0; i < arr.length; i++){
            for(int j=1; j < (arr.length-i); j++){
                if(arr[j-1] > arr[j]){
                    temp = arr[j-1];
                    arr[j-1] = arr[j];
                    arr[j] = temp;
                }

            }
        }

        // строим матрицу из массива
        k = 0;
        for (int i = 0; i < twoDimArr.length; i++) {
            for (int j = 0; j < twoDimArr[i].length; j++) {
                twoDimArr[i][j] = arr[k];
                k++;
            }
        }

        return twoDimArr;
    }

    // сорировка столбцов массива (задание 3 и 4)
    static double[][] sort3(double[][] twoDimArr) {
        // создаем доп строку для хранения сумм столбцов
        double[][] SumTwoDimArr  = new double[twoDimArr.length+1][twoDimArr[0].length];
        double columnSum = 0;
        //вычисляем сумму элементов для каждого столбца
        for (int i = 0; i < SumTwoDimArr[0].length; i++) {
            columnSum = 0;
            for (int j = 0; j < SumTwoDimArr.length; j++) {
                if (j < SumTwoDimArr.length-1) {
                    SumTwoDimArr[j][i] = twoDimArr[j][i];
                    columnSum += SumTwoDimArr[j][i];
                } else {
                    SumTwoDimArr[j][i] = columnSum;

                }
            }
        }


// сортируем массив
        double temp = 0;

        for (int i = 0; i < SumTwoDimArr[0].length; i++) {
            for (int k = i + 1; k < SumTwoDimArr[0].length; k++) {
                if (SumTwoDimArr[SumTwoDimArr.length-1][i] < SumTwoDimArr[SumTwoDimArr.length-1][k]) {
                    for (int j = 0; j < SumTwoDimArr.length; j++) {
                        temp = SumTwoDimArr[j][i];
                        SumTwoDimArr[j][i] = SumTwoDimArr[j][k];
                        SumTwoDimArr[j][k] = temp;
                    }
                }
            }
        }
        return SumTwoDimArr;
    }

    //сотрировка строк массива(задание 5 и 6)
    static double[][] sort5(double[][] twoDimArr) {
        // создаем доп столбец для хранения сумм строк
        double[][] SumTwoDimArr = new double[twoDimArr.length][twoDimArr[0].length + 1];
        double rowSum = 0;
        //вычисляем сумму элементов для каждой строки
        for (int i = 0; i < SumTwoDimArr.length; i++) {
            rowSum = 0;
            for (int j = 0; j < SumTwoDimArr[i].length; j++) {
                if (j < SumTwoDimArr.length - 1) {
                    SumTwoDimArr[i][j] = twoDimArr[i][j];
                    if (j%2 == 0) {
                        rowSum += SumTwoDimArr[i][j];
                    }
                } else {
                    SumTwoDimArr[i][j] = rowSum;
                }
            }
        }
// сортируем массив
        double temp = 0;

        for (int i = 0; i < SumTwoDimArr.length; i++) {
            for (int k = i + 1; k < SumTwoDimArr.length; k++) {
                if (SumTwoDimArr[i][SumTwoDimArr[i].length-1] > SumTwoDimArr[k][SumTwoDimArr[i].length-1]) {
                    for (int j = 0; j < SumTwoDimArr.length; j++) {
                        temp = SumTwoDimArr[i][j];
                        SumTwoDimArr[i][j] = SumTwoDimArr[k][j];
                        SumTwoDimArr[k][j] = temp;
                    }
                }
            }
        }
        return SumTwoDimArr;
    }

    // поиск количества строк с простыми числами (задание 7)
    static int sort7(int[][] twoDimArr) {

        int simpleNumberRows = 0;
        for (int i = 0; i < twoDimArr.length; i++) {
            for (int j = 0; j < twoDimArr[i].length; j++) {
                if (isSimpleNumber(twoDimArr[i][j])) {
                    simpleNumberRows++;
                    break;
                }
            }
        }

        System.out.print(simpleNumberRows);
        return simpleNumberRows;
    }

    // поиск количества столбцов с простыми числами (задание 8)
    static int sort8(int[][] twoDimArr) {

        int simpleNumberColumns = 0;
        for (int i = 0; i < twoDimArr[0].length; i++) {
            for (int j = 0; j < twoDimArr.length; j++) {
                if (isSimpleNumber(twoDimArr[j][i])) {
                    simpleNumberColumns++;
                    break;
                }
            }
        }

        System.out.print(simpleNumberColumns);
        return simpleNumberColumns;
    }

    // метод определения простых чисел
    static boolean isSimpleNumber (int number) {
        boolean flag = true;

        if(number==1) { // 1 - не простое число

            flag = false;
        }

        for(int d=2; d*d<=number; d++){
            // если разделилось нацело, то составное
            if(number%d==0) {

                flag = false;
                break;
            }

        }
        // если нет нетривиальных делителей, то простое

        return flag;
    }

    // умножение матриц (задание 9)
    static double[][] matrixMult(double[][] twoDimArr, double[][] twoDimArr2 ) {


        if ((twoDimArr[0].length != twoDimArr2.length)) {
            throw new IllegalArgumentException("Умножение не возможно. Матрицы не согласованны");
        }
        int rows1 = twoDimArr.length; // строки первой матрицы
        int cols2 = twoDimArr2[0].length;// столбцы второй матрицы
        int cols1 = twoDimArr[0].length; // столбцы первой матрицы
        double[][] resMatrix = new double[rows1][cols2];



        for (int i = 0; i < rows1; i++) {
            for (int j = 0; j < cols2; j++) {
                for(int k = 0; k < cols1; k++) {
                    resMatrix[i][j] += twoDimArr [i][k] * twoDimArr2[k][j];
                }
            }
        }
        return resMatrix;
    }
}
