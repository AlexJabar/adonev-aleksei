package Training;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;

public class Main {
    public static void main(String[] args) {
        // task1();
        // task2();
        // task3();
        // task4();
        // task5();
        //System.out.println(task6(999999999));


    }

    static void task1() {
        int x = 2;
        int end = 1000;
        while (x < end) {
            if (x % 2 != 0) {
                System.out.print(x+"   "+(x+2));
                System.out.println();
                x = x+2;
            }
            x++;
        }
    }

    static void task2() {
        int n = 45734587;
        int sum = 0;
        int count = 0;

        while (n > 0) {
            sum += n%10;
            n /= 10;
            count++;
        }

        System.out.println(sum+"   "+count);
    }

    static void task3() {
        int n = 58493;
        ArrayList<Integer> digits = new ArrayList<>();

        while (n > 0) {
            digits.add(n%10);
            n /= 10;
        }

        int j = digits.size();
        int sum = 0;
        for (int i = 0; i < digits.size(); i++, j--) {
            sum += digits.get(i) * Math.pow(10,j-1);
        }
        System.out.println(sum);
    }

    static void task4() {

        int start = 1;
        int end = 99999;
        int n;
        int flag;

        while (start < end) {

        flag = 0;
        ArrayList<Integer> digits = new ArrayList<>();
        n = start;
            while (n > 0) {
                digits.add(n%10);
                n /= 10;
            }
            int j = digits.size()-1;
            int count = 0;

            for (int i = 0; i < digits.size()/2 ; i++, j--) {
                if (digits.get(i) != digits.get(j)) {
                    flag = 1;
                    break;
                }
            }

            if (flag != 1) {
                System.out.println(start+" палиндром");
            }

            start++;
        }
    }

    static void task5() {
        int n = 123123; // 3 1 3 1
        int number = n;
        int digitNum = (int)(Math.log10(n)+1);
        if (digitNum % 2 != 0) {
            System.out.println("Число "+number+" не симметричное");
            System.exit(1);
        }

        ArrayList<Integer> digits = new ArrayList<>();
        while (n > 0) {
            digits.add(n%10);
            n /= 10;
        }
        int sum  = 0;
        int sum2 = 0;
        int j = digits.size()/2;

        for (int i = 0; i < digits.size() ; i++, j--) {
            if ( i < digits.size()/2) {
                sum += digits.get(i) * Math.pow(10,j-1);
            } else {
                if (j==0) j = digits.size()/2;
                sum2 += digits.get(i) * Math.pow(10,j-1);
            }
        }

        if (sum == sum2) {
            System.out.println("Число "+number+" симметричное");
        } else System.out.println("Число "+number+" не симметричное");
    }

    static int task6(int n) {

        if (n < 10) {
            return n;
        }
            int sum = 0;

            while (n > 0) {
                sum += n % 10;
                n /= 10;
            }

            return task6(sum);
    }

}
