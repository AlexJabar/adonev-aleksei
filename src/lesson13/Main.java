package lesson13;

public class Main {

    public static void main(String[] args) {
        //Boolean[] arr = {true,false,true};
        //Array1.transform(arr);
        int a = Symb.code('a');
        System.out.print(a);
    }

}



class Array1
{
static <T> T[] transform(T[] arr) {
    if (arr instanceof Boolean[]) {
        Boolean[] boolArr = new Boolean[arr.length];
        System.arraycopy(arr, 0, boolArr, 0,arr.length );
        return (T[]) boolArr;
    } else return null;

}

}

class Symb
{
    static <T> int code(T symb) {
        if (symb instanceof Character) {
            return symb.hashCode();
        }
        return 0;
    }
}





