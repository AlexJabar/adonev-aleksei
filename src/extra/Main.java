package extra;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;

public class Main {
    public static void main(String[] args) {

        //task1();

        Triangle tri = new Triangle(5,6,8);
        System.out.println(tri.Geron());
        //task4("max(min(1,7),max(6,5))");

        /*Clock clock = new Clock();
        try {
            clock.setTime(15, 0, 0);
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
        }
        clock.ticTac();*/

       /* User vova = new User(3587323, 37577, "alex", "boban",
                                "vovan", "rehov str. 12", 500,600,
                                        1000, 150);
        User marik = new User(2343, 346546577, "gdfg", "bdffgn",
                "vsdfsdfn", "redfsdfsdf200,100,",
                650, 250,44, 1545);

        User varik = new User(35654575, 3457575, "dfgdfg", "bobfgh5an",
                "vovsdfff3an", "dsfsdf12", 5000,6000,
                910, 1500);

        UserBook book = new UserBook();
        book.addUser("Vova", vova);
        book.addUser("Marik", marik);
        book.addUser("Varik", varik);
        book.search();*/

    }

    static void task2() {
        Random rand = new Random();
        Integer[] arr = {2, 4, 6, 22, 1, 5, 0, 0};
        Integer[] newArr = new Integer[arr.length];
        int randIndex;

        for (int i = 0; i < arr.length; i++) {
            randIndex = rand.nextInt(arr.length);

            while (randIndex == i || newArr[randIndex] != null) {
                randIndex = rand.nextInt(arr.length);
            }

            newArr[randIndex] = arr[i];
        }
        for (int i = 0; i < newArr.length ; i++) {
            System.out.print(newArr[i]+"  ");
        }
    }

    static void task5() {
        Scanner in = new Scanner(System.in);
        System.out.println("Введите размерность матрицы:");
        int size = in.nextInt();

        int[][] matrix = new int[size][size];

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length ; j++) {
                if (i == j) {
                    matrix[i][j] = 3;
                } else if (j > i) {
                    matrix[i][j] = 2;
                } else matrix[i][j] = 1;
            }
        }
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                System.out.print(matrix[i][j]+"  ");
            }
            System.out.println();
        }

    }

    static void task6() {
        Scanner in = new Scanner(System.in);
        Random rand = new Random();
        System.out.println("Введите размерность матрицы:");
        int size = in.nextInt();

        int[][] matrix = new int[size][size];

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length ; j++) {
                if (i == j) {
                    matrix[i][j] = rand.nextInt(9 - 1 + 1) + 1;
                } else matrix[i][j] = 1;
            }
        }

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                System.out.print(matrix[i][j]+"  ");
            }
            System.out.println();
        }
    }

    static void task3() {
        String str = "  Alef beT   Gimek DaLeT   ";
        str = str.replaceAll(" +"," ");
        str = str.trim();
        str = str.toLowerCase();

        str = str.replaceAll("(?<= |^)[a-z]", "$0".toUpperCase());
        System.out.println(str);

    }

    static void task4(String formula) {

        if (formula.matches("^(max|min)\\([0-9]+,[0-9]+\\)")) {
            System.out.println(parser(formula));

        } else {
            String regex = "(?<=[\\s\\S])(max|min)\\([0-9],[0-9]\\)(?=[\\s\\S]+)";

            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(formula);

            if (matcher.find())
            {
                formula = formula.replaceFirst(regex, parser(matcher.group(0)));
                task4(formula);
            }
        }
    }

    static String parser(String formula) {
        int a = Integer.parseInt(formula.substring(4,5));
        int b = Integer.parseInt(formula.substring(6,7));

        if (formula.matches("^max\\([0-9]+,[0-9]+\\)")) {
            int max = Math.max(a,b);
            return String.valueOf(max);
        } else if (formula.matches("^min\\([0-9]+,[0-9]+\\)")) {
            int min = Math.min(a,b);
            return String.valueOf(min);
        } else return "";
    }

    static void task1() {
        int n = 10;
        int k = 4;
        Random rand = new Random();
        Scanner in = new Scanner(System.in);
        int number = rand.nextInt(n-1+1)+1;
        int i = 0;
        while (i < k) {
            int input = in.nextInt();
            if (input == number) {
                System.out.println("число угадано!");
                break;
            } else {
                i++;
                if (input > number) {
                    System.out.println("число меньше!");
                } else System.out.println("число больше!");
            }
        }
    }

}


class Clock {
    int hour = 0;
    int min = 0;
    int sec = 0;
    Timer timer = new Timer();
    String input = "";


    public void ticTac() {
        Thread inputThread = new Thread(new Runnable() {
            public void run()
            {
                Scanner in = new Scanner(System.in);
                while (true) {
                    input = in.next();
                    if (input.equals("s")) break;
                    if (input.equals("h")) hour++;
                    if (input.equals("m")) min++;
                }

            }
        });

        inputThread.start();

        timer.schedule(new TimerTask() {
            @Override
            public void run() {

                if (input.equals("s")) {
                    timer.cancel();
                    timer.purge();
                    return;
                }

                sec++;
                if (sec >= 60) {
                    sec = 0;
                    min++;
                }

                if (min >= 60) {
                    min = 0;
                    hour++;
                }

                if (hour >= 24) {
                    hour = 0;
                }

                System.out.println(hour+":"+min+":"+sec);
            }
        }, 0, 2000);
    }

    public void setTime(int hour, int min, int sec) {
        if (hour > 59 || min > 59 || sec > 59 ||
            hour <  0 || min <  0 || sec <  0) {
            throw new IllegalArgumentException("Wrong time format");
        }

        this.hour = hour;
        this.min  = min;
        this.sec  = sec;
    }

}


class UserBook
{
    HashMap<String, User> userBook = new HashMap<String, User>();

    public void addUser(String name,User user) {
        userBook.put(name, user);
    }

    public void print() {
        for (String key: userBook.keySet()) {
            System.out.println(key+":");
            System.out.println(userBook.get(key).getId()+"   "+userBook.get(key).getCardId());
            System.out.println(userBook.get(key).getName()+"   "+userBook.get(key).getSurname());
            System.out.println(userBook.get(key).getFatherName()+"   "+userBook.get(key).getAdress());
            System.out.println(userBook.get(key).getDebet()+"   "+userBook.get(key).getCredit());
            System.out.println(userBook.get(key).getIntercityTime()+"   "+userBook.get(key).getCityTime());
            System.out.println();

        }
    }




    public void search() {
        for (String key: userBook.keySet()) {
            if (userBook.get(key).getIntercityTime() > 100) {
                System.out.println(key+":");
                System.out.println(userBook.get(key).getId()+"   "+userBook.get(key).getCardId());
                System.out.println(userBook.get(key).getName()+"   "+userBook.get(key).getSurname());
                System.out.println(userBook.get(key).getFatherName()+"   "+userBook.get(key).getAdress());
                System.out.println(userBook.get(key).getDebet()+"   "+userBook.get(key).getCredit());
                System.out.println(userBook.get(key).getIntercityTime()+"   "+userBook.get(key).getCityTime());
                System.out.println();
            }
        }
    }
}

class User
{
    private long id, cardId;
    private String name, surname, fatherName, adress;
    private int debet,credit;
    private int intercityTime, cityTime;

    User(long id, long cardId, String name, String surname, String fatherName, String adress, int debet, int credit, int intercityTime, int cityTime) {
        this.id = id;
        this.cardId = cardId;
        this.name = name;
        this.surname = surname;
        this.fatherName = fatherName;
        this.adress = adress;
        this.debet = debet;
        this.credit = credit;
        this.intercityTime = intercityTime;
        this.cityTime = cityTime;
    }

    public void setCardId(long cardId) {
        this.cardId = cardId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public void setDebet(int debet) {
        this.debet = debet;
    }

    public void setCredit(int credit) {
        this.credit = credit;
    }

    public void setIntercityTime(int intercityTime) {
        this.intercityTime = intercityTime;
    }

    public void setCityTime(int cityTime) {
        this.cityTime = cityTime;
    }

    public long getId() {
        return id;
    }

    public long getCardId() {
        return cardId;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getFatherName() {
        return fatherName;
    }

    public String getAdress() {
        return adress;
    }

    public int getDebet() {
        return debet;
    }

    public int getCredit() {
        return credit;
    }

    public int getIntercityTime() {
        return intercityTime;
    }

    public int getCityTime() {
        return cityTime;
    }


}


class Triangle {
    double sideA ;
    double sideB ;
    double sideC ;

     Triangle(int sideA, int sideB, int sideC) {
        this.sideA = sideA;
        this.sideB = sideB;
        this.sideC = sideC;
    }

    public double Geron() {
         double p = (sideA + sideB + sideC)/2;
         return Math.sqrt(p*(p-sideA)*(p-sideB)*(p-sideC));
    }
}





