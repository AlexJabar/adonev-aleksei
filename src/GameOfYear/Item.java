package GameOfYear;

public class Item {
    String name;
    int bonusHp;
    int bonusDamage;
    int bonusArmor;

    public Item(String name,int bonusHp, int bonusDamage, int bonusArmor) {
        this.name = name;
        this.bonusHp = bonusHp;
        this.bonusDamage = bonusDamage;
        this.bonusArmor = bonusArmor;
    }
}
