package GameOfYear;

public class Player extends Unit {



    Player(int x, int y){
        super(100,1,5);
        this.x = x;
        this.y = y;
        numOfDraw = 1;
    }

    @Override
    void move(Direction direction) {
        x+=direction.x;
        y+=direction.y;
    }
}
