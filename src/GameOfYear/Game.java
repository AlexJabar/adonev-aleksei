package GameOfYear;

import java.util.Random;
import java.util.Scanner;

public class Game {
    static int sizeMap = 10;
    int[][] map = new int[sizeMap][sizeMap];
    Player player = new Player(1, 4);
    Goblin goblin = new Goblin(10, 1, 20);
    Goblin goblin1 = new Goblin(20, 1, 20);
    Goblin goblin2 = new Goblin(30, 1, 50);

    int countOfObj = 0;
    Unit[] allObj = new Unit[10];


    void createAllObj() {
        allObj[countOfObj] = player;
        countOfObj++;
        allObj[countOfObj] = goblin;
        countOfObj++;
        allObj[countOfObj] = goblin1;
        countOfObj++;
        allObj[countOfObj] = goblin2;
        countOfObj++;
    }

    void battle(Unit first, Unit second) {
        int actionPlayer = 0;
        int actionEnemy = 0;

        Scanner scanner = new Scanner(System.in);
        Random random = new Random();

        while (first.getHp() > 0 && second.getHp() > 0) {
            System.out.println("Chose your action");
            System.out.println("1 - strike");
            System.out.println("2 - defence");
            actionPlayer = scanner.nextInt();
            actionEnemy = random.nextInt(2) + 1;

            System.out.println("Player's hp = " + first.getHp());
            System.out.println("Second's hp = " + second.getHp());
            first.setHp(first.getHp() - (second.getDamage(actionEnemy) - first.getArmor(actionPlayer)));
            second.setHp(second.getHp() - (first.getDamage(actionPlayer) - second.getArmor(actionEnemy)));

        }

        second.numOfDraw = 4;
    }

    int checkCollisoin(Unit[] allObj) {
        for (int i = 1; i < allObj.length; i++) {
            if (allObj[i] != null)
                if (player.x == allObj[i].x && player.y == allObj[i].y) {
                    return i;
                }
        }
        return 0;
    }

    void putObjectOnMap(int x, int y, int obj) {
        map[y][x] = obj;
    }

    void clearMap() {
        for (int i = 0; i < map.length; i++) {
            for (int j = 0; j < map[i].length; j++) {
                map[i][j] = 0;
            }
        }
    }

    void update() {
        int objToAction = checkCollisoin(allObj);
        if (objToAction != 0) {
            battle(allObj[0], allObj[objToAction]);
            if (allObj[objToAction].getHp() <= 0) {
                System.out.println("We found corpse of goblin");
            } else {
                System.out.println("we found goblin!");
            }
        }
    }


    void draw() {
        clearMap();
        for (int i = 0; i < allObj.length; i++) {
            if (allObj[i] != null /*&& allObj[i].getHp() > 0*/) {
                putObjectOnMap(allObj[i].x, allObj[i].y, allObj[i].numOfDraw);
            }
        }
        System.out.println("Player's hp = " + player.getHp());
        System.out.println("Player's armor = " + player.getArmor(1));
        System.out.println("Player's damage = " + player.getDamage(1));
        for (int i = 0; i < map.length; i++) {
            for (int j = 0; j < map[i].length; j++) {
                System.out.print(map[i][j]);
            }
            System.out.println();
        }
    }

    Game() {
        clearMap();
        createAllObj();
        Item sword = new Item("Sword",0,30,0);
        player.putIntoInventory(sword);
    }

    /***
     * игровой цикл
     */
    void start() {
        Scanner scanner = new Scanner(System.in);
        int action = 0;
        while (true) {
            if (player.getHp() <= 0) {
                System.out.println("Oh, no... WASTED");
                break;
            }
            draw();
            update();
            action = scanner.nextInt();
            switch (action) {
                case 2:
                    if (player.canGo(Direction.DOWN, sizeMap)) {
                        player.move(Direction.DOWN);
                    }
                    break;
                case 4:
                    if (player.canGo(Direction.LEFT, sizeMap)) {
                        player.move(Direction.LEFT);
                    }
                    break;
                case 6:
                    if (player.canGo(Direction.RIGHT, sizeMap)) {
                        player.move(Direction.RIGHT);
                    }
                    break;
                case 8:
                    if (player.canGo(Direction.UP, sizeMap)) {
                        player.move(Direction.UP);
                    }
                    break;
                default:

            }
            Random random = new Random();
            goblin.move(Direction.values()[random.nextInt(Direction.values().length)]);
        }
    }
}
