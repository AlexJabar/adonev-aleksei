package GameOfYear;

public abstract class Unit {
    private int hp;
    private int armor;
    private int damage;
    int x;
    int y;
    int numOfDraw;

    Item[] inventory = new Item[3];
    int contOfFree = 0;

    void putIntoInventory(Item item) {
        inventory[contOfFree] = item;
        contOfFree++;
    }



    public int getArmor() {
        return armor;
    }

    public int getDamage() {
        return damage;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    boolean canGo(Direction direction, int sizeMap) {
        int newX = x + direction.x;
        int newY = y + direction.y;
        if (newX < sizeMap && newX >= 0 && newY < sizeMap && newY >= 0)
            return true;
        return false;
    }

    int getDamage(int action) {
        if (action == 1) {
            int allDamage = damage;
            for (Item i : inventory) {
                allDamage += i.bonusDamage;
            }
            return allDamage;
        }
        if (action == 2) {
            int allDamage = damage;
            for (Item i : inventory) {
                allDamage += i.bonusDamage;
            }
            return allDamage / 2;
        }

        return -1;
    }

    int getArmor(int action) {
        if (action == 1) {
            return 0;
        }
        if (action == 2) {
            int allArmor = armor;
            for (Item i : inventory) {
                allArmor+=i.bonusArmor;
            }
            return allArmor;
        }
        return -1;
    }

    public Unit(int hp, int armor, int damage) {
        this.hp = hp;
        this.armor = armor;
        this.damage = damage;
        for (int i = 0; i < inventory.length; i++) {
            inventory[i] = new Item("empty",0,0,0);
        }
    }

    abstract void move(Direction direction);

}
