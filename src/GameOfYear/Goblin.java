package GameOfYear;

import java.util.Random;

public class Goblin extends Unit {


    public Goblin(int hp, int armor, int damage) {
        super(hp, armor, damage);
        Random random = new Random();
        numOfDraw = 7;
        this.x = random.nextInt(10);
        this.y = random.nextInt(10);
    }

    @Override
    void move(Direction direction) {

        x+= direction.x;
        y+= direction.y;
    }
}
