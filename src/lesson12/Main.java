package lesson12;


import java.time.DayOfWeek;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        BookOfMeeting book = new BookOfMeeting();
        int m = 0;
        int d = 1;
        int dow = 0;
        Date date;

        while (m < 12) {
            while (d <= Months.values()[m].getNumber()) {
                if (dow > 6) {
                    dow = 0;
                }
                date = new Date(Months.values()[m], Days.values()[dow], d);
                book.setDate(date);
                d++;
                dow++;
            }
            m++;
            d = 1;
        }
        book.addMeet(Months.AUG, 7, 7, 7);
        book.addMeet(Months.FEB, 28, 7, 7);
        book.addMeet(Months.FEB, 28, 7, 7);
        book.showMeets();
    }
}


class DateOfMeet {
    private Days day;
    private Months month;
    private String meetingName;
    private String meetingDesc;
    private int hour;
    private int min;
    private int date;

    public void show() {
        System.out.println("У вас встеча " + this.meetingName +
                " .Инфо: " + this.meetingDesc + ". " + this.date + " "
                + this.month + " в " + this.day + " в " + this.hour + ":" + this.min);
    }


    public DateOfMeet(Days day, Months month, String meetingName, String meetingDesc, int hour, int min, int date) {
        this.day = day;
        this.month = month;
        this.meetingName = meetingName;
        this.meetingDesc = meetingDesc;
        this.hour = hour;
        this.min = min;
        this.date = date;
    }
}

enum Days {
    MON,
    TUS,
    WED,
    THU,
    FRI,
    SAT,
    SUN;
}

enum Months {


    JAN(31), FEB(28), MAR(31), APR(30), MAY(31), JUN(30), JUL(31), AUG(31), SEP(30), OCT(31), NOV(30), DEC(31);

    private int number;

    Months(int number) {
        this.number = number;
    }

    public int getNumber() {
        return number;
    }
}

class BookOfMeeting {
    public Date[] dates;
    public int dateNumber = 0;

    public BookOfMeeting() {
        this.dates = new Date[365];
    }

    public void setDate(Date date) {
        this.dates[this.dateNumber] = date;
        this.dateNumber++;
    }

    public void addMeet(Months months, int data, int hour, int min) {
        for (int i = 0; i < this.dates.length; i++) {
            if (dates[i].getMonths() == months && dates[i].getData() == data) {
                if (dates[i].getHour() == -1 && dates[i].getMin() == -1) {
                    dates[i].setHour(hour);
                    dates[i].setMin(min);
                    break;
                } else {
                    System.out.println("Эта дата уже занята");
                    break;
                }
            }


        }
    }

    public void showMeets() {
        for (int i = 0; i < this.dates.length; i++) {
            if (dates[i].getHour() != -1 && dates[i].getMin() != -1) {
                System.out.println("У вас запланированны след. встречи:");
                System.out.println(dates[i].getData()+" "+dates[i].getMonths()+" в "+dates[i].getDay()+" в "+dates[i].getHour()+":"+dates[i].getMin());
            }
        }
    }
}

class Date {
    private Months months;
    private Days day;
    private int data;
    private int hour = -1;
    private int min = -1;

    public Date(Months months, Days day, int data) {
        this.months = months;
        this.day = day;
        this.data = data;
    }

    public Months getMonths() {
        return months;
    }

    public Days getDay() {
        return day;
    }

    public int getData() {
        return data;
    }

    public int getHour() {
        return hour;
    }

    public int getMin() {
        return min;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public void setMin(int min) {
        this.min = min;
    }
}

