package Lesson16;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        func(in);
    }


    public static void func(Scanner in) {
        double minX = in.nextFloat();
        double maxX = in.nextFloat();
        double minY = in.nextFloat();
        double maxY = in.nextFloat();
        double res = 0;

        for (double x = minX; x < maxX; x=x+1) {
            try {
                res = 18 * Math.pow(x, 2) + (54 / x) - 8;
                if (res < minY || res > maxY) {
                    System.out.println(res);
                    throw new IntervalException("Ответ не входит в заданный промежуток");
                } else
                    System.out.println(res);
            } catch (IntervalException e ) {
                System.out.println(e);
                continue;
            }

        }

    }
}

class IntervalException extends Exception
{
    public IntervalException(String message) {
        super(message);
    }
}
