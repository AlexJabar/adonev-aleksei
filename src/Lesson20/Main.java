package Lesson20;



import java.util.ArrayList;


public class Main {
    public static void main(String[] args) {
        //int n = 0;
        //int i = 1;
        /*while (n < 4) {
            if (task5(i)) {
                System.out.println(i);
                n++;
                i++;
            } else i++;
        }*/

        task4();
    }


    static void task3() {

        for (int i = 1; i <= 10; i++) {
            for (int j = 1; j <= 20; j++) {
                for (int k = 1; k <= 200 ; k++) {
                    if (i * 10 + j * 5 + k * 0.5 == 100) {
                        if (i+j+k == 100) {
                            System.out.println(i + " " + j + " " + k);
                        }
                    }
                }
            }
        }

    }

    static void task4() {
        char[][] field = new char[100][100];
        int x=50;
        int y = 50;
        int n = 1;
        int f= 1;
        field[x][y] = '*';
       while (n < 9000) {

           for (int i = 0; i < f; i++) {
               y++;
               n++;
               if (isPrime(n)) {
                   field[x][y] = '*';
               } else field[x][y] = ' ';
           }

           for (int i = 0; i < f; i++) {
               x--;
               n++;
               if (isPrime(n)) {
                   field[x][y] = '*';
               } else field[x][y] = ' ';
           }

           f++;

           for (int i = 0; i < f ; i++) {
               y--;
               n++;
               if (isPrime(n)) {
                   field[x][y] = '*';
               } else field[x][y] = ' ';
           }

           for (int i = 0; i < f ; i++) {
               x++;
               n++;
               if (isPrime(n)) {
                   field[x][y] = '*';
               } else field[x][y] = ' ';
           }
           f++;
       }

        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field.length ; j++) {
                System.out.print(field[i][j]+"");
            }
            System.out.println();
        }
    }

    static void task6() {
        int n = 9000;
        int sum =0;
        for (int i = 1; i < n; i++) {
            for (int j = 1; j < (i/2)+1; j++) {
                if (i%j == 0) {
                    sum+=j;
                }
            }
            if (sum == i) {
                System.out.println(sum);
            }
            sum=0;
        }
    }
    
    static boolean task5(int n) {


       int length = (int)(Math.log10(n)+1);
       int[] dig = new int[length];
        for (int i = 1; i <= length ; i++) {
            dig[i-1] = (int)((n%Math.pow(10,i))/Math.pow(10,i-1));
        }

        int fact = 1;
        int sumFact = 0;
        for (int i = 0; i < dig.length; i++) {
            for (int j = 1; j <= dig[i]; j++) {
                fact*=j;
            }
            sumFact += fact;
            fact =1;
        }

        if (sumFact == n) {
            return true;
        } else return false;

    }
    
    static void task1() {
        int n = 150;
        for (int i = 1; i <= n ; i++) {
            for (int j = 1; j <= n ; j++) {
                for (int k = 1; k <= n ; k++) {
                    if (i*i + j*j == k*k) {
                         int nod = god(god(i,j),k);
                         if(nod == 1) {
                             System.out.println(i+"  "+j+"  "+k);
                         }
                    }
                }
            }
        }
    }

    static void task2() {

        int s = 0;
        for (int i = 1; i <= 150 ; i++) {
            for (int j = 1; j <= 150 ; j++) {
                for (int k = 1; k <= 150 ; k++) {
                    if (i*i + j*j == k*k) {
                        s = (i*j)/2;
                        if (s <= 280) {
                            System.out.println(i+"  "+j+"  "+k+" - "+s);
                        }
                    }
                }
            }
        }
    }

    static int god(int a, int b) {
        while (a != b) {
            if (a > b) {
                a = a - b;
            } else {
                b = b - a;
            }
        }
        return a;
    }


    public static boolean isPrime(int N) {
        if (N < 2) return false;
        for (int i = 2; i*i <= N; i++)
            if (N % i == 0) return false;
        return true;
    }

}


class Triangle {
    double sideA ;
    double sideB ;
    double sideC ;

    Triangle(int sideA, int sideB, int sideC) {
        this.sideA = sideA;
        this.sideB = sideB;
        this.sideC = sideC;
    }

    public int geron() {
        double p = (sideA + sideB + sideC)/2;
        return (int) Math.sqrt(p*(p-sideA)*(p-sideB)*(p-sideC));
    }
}

