package lesson8;



import java.util.Arrays;
import java.util.Comparator;
import java.util.InputMismatchException;
import java.util.Collections;

public class Main {
    public static void main(String[] args) {
        Library library = new Library(10);
        Book book1 = new Book("Женщины", "Буковски", "1978");
        Book book2 = new Book("Атлант расправил плечи", "Рэнд", "1957");
        Book book3 = new Book("Кортик", "Рыбаков", "1948");
        Book book4 = new Book("Яшка", "Рыбаков", "1948");
        Book book5 = new Book("Молот", "Рыбаков", "1948");
        library.addBook(book1);
        library.addBook(book2);
        library.addBook(book3);
        library.addBook(book4);
        library.addBook(book5);
        library.sortBooks();
        library.deleteBook("Женщины");
        //library.sortBooks();
        library.showBooks();


        /*try { // поиск по названию, автору или дате выхода
           library.findBook("рыбаков").writeName();
        } catch (IllegalArgumentException e) {
            System.out.println(e);
        }*/

    }

}

class Library  {
    public int booksAmont;
    public Book[] booksList;
    public int bookNumber = 0;

    Library(int booksAmount) {
        this.booksAmont = booksAmount;
        this.booksList = new Book[booksAmont];
    }




    public void addBook(Book book) {
        this.booksList[bookNumber] = book;
        this.bookNumber++;
    }

    public void deleteBook(String bookName){
        bookName = bookName.toLowerCase();
        for (int i = 0; i < this.bookNumber; i++) {
            if (this.booksList[i].getName().toLowerCase().equals(bookName)) {// если есть книга с таким названием
                for (int j = i+1; j < this.bookNumber; j++) { // тогда заменяем ее на следующую книгу в библиотеке и так даллее
                    this.booksList[j-1] = this.booksList[j];

                }
                this.bookNumber--; // теперь у нас -1 книга в библиотеке
                this.booksList[this.bookNumber] = null;
                break;
            }
        }


    }

    public void sortBooks() {
        Book temp;
        for(int i=0; i < this.bookNumber; i++){
            for(int j=1; j < (this.bookNumber-i); j++){
                if(this.booksList[j-1].getName().compareToIgnoreCase(this.booksList[j].getName()) > 0){ // сравниваем строки по значению Unicode
                    temp = this.booksList[j-1];
                    this.booksList[j-1] = this.booksList[j];
                    this.booksList[j] = temp;
                }

            }
        }
    }



    public Book findBook(String bookProp) {
        bookProp = bookProp.toLowerCase();
        for (int i = 0; i < this.bookNumber; i++) {
            if (this.booksList[i].getName().toLowerCase().equals(bookProp) ||
                    this.booksList[i].getAuthor().toLowerCase().equals(bookProp) ||
                    this.booksList[i].getDate().toLowerCase().equals(bookProp)) {
                return this.booksList[i];
            }
        }

        throw new IllegalArgumentException("Книга "+bookProp+" не найдена");
    }


    public void showBooks() {
        for (Book book : this.booksList) {
            if (book != null) {
                System.out.println(book.getName());
            }
        }

    }


}

class Book {
    private String name; // Название
    private String author; // Автор
    private String date;// дата

    Book(String name, String author, String date) {
        this.name = name;
        this.author = author;
        this.date = date;
    }

    public String getName() {
        return this.name;
    }

    public String getAuthor() {
        return this.author;
    }

    public String getDate() {
        return this.date;
    }

    public void writeName() {
        System.out.println(this.name+",  "+this.author+",  "+this.date);
    }


}


