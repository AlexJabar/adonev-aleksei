package catmouse;


import java.util.Scanner;
import java.util.Random;


public class Main {
    public static int n = 12;
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Unit player = new Unit();
        Chaser chaser = new Chaser();
        boolean running = true;

    while (running) {
        drawField(player,chaser);
        System.out.println("Выбери направление хода");
        Direction input;
        try {
            input = Direction.valueOf(in.next());
        } catch (IllegalArgumentException e) {
            System.out.println("only l, r, u , d");
            continue;
        }

        player.move(input);



        Direction direction = chaser.findWay(player);
        chaser.move(direction);

        if (player.x == chaser.x && player.y == chaser.y) {
            running = false;
        }
    }

    System.out.println("game over");

    }

    public static void drawField(Unit unit, Chaser chaser) {
        char[][] field = new char[Main.n][Main.n];



        for (int i = 0; i < Main.n; i++) {
            for (int j = 0; j < Main.n; j++) {
                if (i == unit.y && j == unit.x) {
                    field[i][j] = '*';
                } else if (i == chaser.y && j == chaser.x) {
                    field[i][j] = '@';
                }
                else field[i][j] = '-';
            }
        }
        for (int i = 0; i < Main.n; i++) {
            for (int j = 0; j < Main.n; j++) {
                System.out.print(field[i][j]+" ");
            }
            System.out.println();
        }

    }
}



class Unit {
    Random rand = new Random();
    protected int x = rand.nextInt((Main.n-1) + 1);
    protected int y = rand.nextInt((Main.n-1) + 1);


    Unit(){}

    protected void move(Direction dir) {

        if (dir == Direction.u) {

            this.y = this.y != 0 ? this.y-1:this.y;
        }
        if (dir == Direction.d) {
            this.y = this.y != Main.n-1 ? this.y+1:this.y;
        }
        if (dir == Direction.r) {
            this.x = this.x != Main.n-1 ? this.x+1:this.x;
        }
        if (dir == Direction.l) {
            this.x = this.x != 0 ? this.x-1:this.x;
        }

    }
}

class Chaser extends Unit
{
    Chaser() {

        this.x = rand.nextInt((Main.n-1) + 1);
        this.y  = rand.nextInt((Main.n-1) + 1);
    }

    public Direction findWay(Unit player) {
       int deltx = Math.abs(this.x - player.x);
       int delty = Math.abs(this.y - player.y);
       double delterr;
       if (deltx != 0) {
              delterr = delty / deltx;
       } else delterr =1;

       if (delterr >= 0.5) {
           if (this.y - player.y > 0) {
               return Direction.u;
           } else return Direction.d;
       } else {
           if (this.x - player.x > 0) {
               return Direction.l;
           } else return Direction.r;
       }

    }





}

enum Direction{
    u,d,r,l;
}


